# Nameday SK HU

This module currently only provides blocks for Slovak and Hungarian namedays.

Service usage:
use Drupal\nameday_skhu\NamedayClass;

$nameday_service = \Drupal::service('nameday');
$nameday = $nameday_service->nameday('hu', '2017', '04', '10');

For a full description of the module, visit the
[project page](https://www.drupal.org/project/nameday_skhu).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/nameday_skhu).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

1. Install as you would normally install a contributed Drupal module. For
further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
2. Enable the nameday block on block layout.


## Configuration

The module has no menu or modifiable settings. There is no configuration.


## Maintainers

- balagan - [balagan](https://www.drupal.org/u/balagan)
- József Dudás - [dj1999](https://www.drupal.org/u/dj1999)
- szato - [balagan](https://www.drupal.org/u/szato)
