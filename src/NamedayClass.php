<?php

/**
 * @file
 * Contains \Drupal\nameday_skhu\NamedayClass.
 */

namespace Drupal\nameday_skhu;

/**
 * Provides a custom class that returns Hungarian and Slovak namedays.
 */
class NamedayClass {

  /**
   * @param string $lang
   * @param $year
   * @param $month
   * @param $day
   * @return mixed
   */
  public function getNameday($lang = 'hu', $year = '', $month = '', $day = '') {
    // If no date parameters are passed, use current date.
    if (empty($month) || empty($day) || empty($year)) {
      $today_date = getdate();
    }
    else {
      $time = strtotime($day . '-' . $month . '-' . $year);
      $today_date = getdate($time);
    }
    $today_month = $today_date['mon']-1;
    $today_day = $today_date['mday']-1;
    $today_yday = $today_date['yday'];

    // Hungarian namedays by month

// January
    $name_hu[] = array
    ("ÚJÉV", "Ábel", "Genovéva", "Titusz", "Simon",
      "Boldizsár", "Attila", "Gyöngyvér", "Marcell",
      "Melánia", "Ágota", "Ernő", "Veronika",
      "Bódog", "Lóránt", "Gusztáv", "Antal", "Piroska",
      "Sára", "Sebestyén", "Ágnes", "Vince", "Zelma",
      "Timót", "Pál", "Vanda", "Angelika", "Károly,",
      "Adél", "Martina", "Marcella");

// February
    $name_hu[] = array
    ("Ignác", "Karolina", "Balázs", "Ráhel", "Ágota",
      "Dóra", "Tódor", "Aranka", "Abigél", "Elvira",
      "Bertold", "Lívia", "Ella, Linda", "Bálint", "Kolos",
      "Julianna", "Donát", "Bernadett", "Zsuzsanna", "Álmos",
      "Eleonóra", "Gerzson", "Alfréd", "Mátyás", "Géza",
      "Edina", "Ákos, Bátor", "Elemér");

// March
    $name_hu[] = array
    ("Albin", "Lujza", "Kornélia", "Kázmér", "Adorján",
      "Leonóra", "Tamás", "Zoltán", "Franciska", "Ildikó",
      "Szilárd", "Gergely", "Krisztián, Ajtony", "Matild",
      "Kristóf", "Henrietta", "Gertrúd", "Sándor", "József",
      "Klaudia", "Benedek", "Beáta", "Emőke", "Gábor",
      "Irén", "Emánuel", "Hajnalka", "Gedeon", "Auguszta",
      "Zalán", "Árpád");

// April
    $name_hu[] = array
    ("Hugó", "Áron", "Buda, Richárd", "Izidor", "Vince",
      "Vilmos, Bíborka", "Herman", "Dénes", "Erhard", "Zsolt",
      "Zsolt, Leó", "Gyula", "Ida", "Tibor", "Tas, Anasztázia",
      "Csongor", "Rudolf", "Andrea", "Emma", "Konrád, Tivadar",
      "Konrád", "Csilla", "Béla", "György", "Márk",
      "Ervin", "Zita", "Valéria", "Péter", "Katalin, Kitti");

// May
    $name_hu[] = array
    ("Fülöp", "Zsigmond", "Tímea", "Mónika", "Györgyi",
      "Ivett", "Gizella", "Mihály", "Gergely", "Ármin",
      "Ferenc", "Pongrác", "Szervác", "Bonifác", "Zsófia",
      "Botond, Mózes", "Paszkál", "Erik", "Ivó, Milán",
      "Bernát, Felícia", "Konstantin", "Júlia, Rita",
      "Dezső", "Eszter", "Orbán", "Fülöp", "Hella",
      "Emil, Csanád", "Magdolna", "Zsanett, Janka",
      "Angéla");

// June
    $name_hu[] = array
    ("Tünde", "Anita, Kármen", "Klotild", "Bulcsú", "Fatime",
      "Norbert", "Róbert", "Medárd", "Félix", "Margit",
      "Barnabás", "Villő", "Antal, Anett", "Vazul", "Jolán",
      "Jusztin", "Laura", "Levente", "Gyárfás", "Rafael",
      "Alajos", "Paulina", "Zoltán", "Iván", "Vilmos",
      "János", "László", "Levente, Irén", "Péter, Pál",
      "Pál");

// July
    $name_hu[] = array
    ("Annamária", "Ottó", "Kornél", "Ulrik", "Sarolta, Emese",
      "Csaba", "Appolónia", "Ellák", "Lukrécia", "Amália",
      "Nóra, Lili", "Izabella", "Jenő", "Őrs", "Henrik",
      "Valter", "Endre, Elek", "Frigyes", "Emília", "Illés",
      "Dániel", "Magdolna", "Lenke", "Kinga, Kincső",
      "Kristóf, Jakab", "Anna, Anikó", "Olga",
      "Szabolcs", "Márta", "Judit", "Oszkár");

// August
    $name_hu[] = array
    ("Boglárka", "Lehel", "Hermina", "Domonkos", "Krisztina",
      "Berta", "Ibolya", "László", "Emőd", "Lőrinc",
      "Zsuzsanna", "Klára", "Ipoly", "Marcell", "Mária",
      "Ábrahám", "Jácint", "Ilona", "Huba", "István",
      "Sámuel", "Menyhért", "Bence", "Bertalan", "Lajos",
      "Izsó", "Gáspár", "Ágoston", "Beatrix", "Rózsa",
      "Erika");

// September
    $name_hu[] = array
    ("Egon", "Rebeka", "Hilda", "Rozália", "Viktor, Lőrinc",
      "Zakariás", "Regina", "Mária", "Ádám", "Nikolett, Hunor",
      "Teodóra", "Mária", "Kornél", "Szeréna", "Enikő",
      "Edit", "Zsófia", "Diána", "Vilhelmina", "Friderika",
      "Máté", "Móric", "Tekla", "Gellért", "Eufrozina",
      "Jusztina", "Adalbert", "Vencel", "Mihály", "Jeromos");

//October
    $name_hu[] = array
    ("Malvin", "Petra", "Helga", "Ferenc", "Aurél",
      "Renáta", "Amália", "Koppány", "Dénes", "Gedeon",
      "Brigitta", "Miksa", "Kálmán", "Helén", "Teréz",
      "Gál", "Hedvig", "Lukács", "Nándor", "Vendel",
      "Orsolya", "Előd", "Gyöngyi", "Salamon", "Bianka",
      "Dömötör", "Szabina", "Simon", "Nárcisz", "Alfonz",
      "Farkas");

// November
    $name_hu[] = array
    ("Marianna", "Achilles", "Győző", "Károly", "Imre",
      "Lénárd", "Rezső", "Zsombor", "Tivadar", "Réka",
      "Márton", "Jónás, Renátó", "Szilvia", "Aliz",
      "Albert, Lipót", "Ödön", "Hortenzia, Gergő",
      "Jenő", "Erzsébet", "Jolán", "Olivér", "Cecília",
      "Kelemen", "Emma", "Katalin", "Virág",
      "Virgil", "Stefánia", "Taksony", "András, Andor");

// December
    $name_hu[] = array
    ("Elza", "Melinda", "Ferenc", "Barbara, Borbála",
      "Vilma", "Miklós", "Ambrus", "Mária", "Natália", "Judit",
      "Árpád", "Gabriella", "Luca", "Szilárda", "Valér",
      "Etelka", "Lázár", "Auguszta", "Viola", "Teofil",
      "Tamás", "Zéno", "Viktória", "Ádám, Éva", "KARÁCSONY",
      "KARÁCSONY", "János", "Kamilla", "Tamás", "Dávid",
      "Szilveszter");


    // Slovak namedays
    $mena = array('Nový rok', 'Alexandra', 'Daniela', 'Drahoslav', 'Andera', 'Antónia', 'Bohuslav(a)', 'Severín', 'Alexej', 'Dáša', 'Malvína', 'Ernest', 'Rastislav', 'Radovan', 'Dobroslav', 'Kristína', 'Nataša', 'Bohdana', 'Drahomíra', 'Dalibor', 'Vincent', 'Zora', 'Miloš', 'Timotej', 'Gejza', 'Tamara', 'Bohuš', 'Alfonz', 'Gašpar', 'Ema', 'Emil',

      'Tatiana', 'Erik/Erika', 'Blažej', 'Veronika', 'Agáta', 'Dorota', 'Vanda', 'Zoja', 'Zdenko', 'Gabriela', 'Dezider', 'Perla', 'Arpád', 'Valentín', 'Pravoslav', 'Ida', 'Miloslava', 'Jaromír', 'Vlasta', 'Lívia', 'Eleonóra', 'Etela', 'Roman(a)', 'Metej', 'Frederik/Frederika', 'Viktor', 'Alexander', 'Zlatica', 'Radomír',

      'Albín', 'Anežka', 'Bohumil/Bohumila', 'Kazimír', 'Fridrich', 'Radoslav', 'Tomáš', 'Alan/Alana', 'Františka', 'Branislav, Bruno', 'Angela, Angelika', 'Gregor', 'Vlastimil', 'Matilda', 'Svetlana', 'Boleslav', 'Ľubica', 'Eduard', 'Jozef', 'Víťazoslav', 'Blahoslav', 'Beňadik', 'Adrián', 'Gabriel', 'Marián', 'Emanuel', 'Alena', 'Soňa', 'Miroslav', 'Vieroslava', 'Benjamín',

      'Hugo', 'Zita', 'Richard', 'Izidor', 'Miroslava', 'Irena', 'Zoltán', 'Albert', 'Milena', 'Igor', 'Július', 'Estera', 'Aleš', 'Justína', 'Fedor', 'Dana/Danica', 'Rudolf', 'Valér', 'Jela', 'Marcel', 'Ervín', 'Slavomír', 'Vojtech', 'Juraj', 'Marek', 'Jaroslava', 'Jaroslav', 'Jarmila', 'Lea', 'Anastázia',

      'Sviatok práce', 'Žigmunt', 'Galina', 'Florián', 'Lesana/Lesia', 'Hermína', 'Monika', 'Ingrida', 'Roland', 'Viktória', 'Blažena', 'Pankrác', 'Servác', 'Bonifác', 'Žofia', 'Svetozár', 'Gizela', 'Viola', 'Gertrúda', 'Bernard', 'Zina', 'Júlia, Juliana', 'Želmíra', 'Ela', 'Urban', 'Dušan', 'Iveta', 'Viliam', 'Vilma', 'Ferdinand', 'Petronela/Petrana',

      'Žaneta', 'Xénia', 'Karolína', 'Lenka', 'Laura', 'Norbert', 'Róbert', 'Medard', 'Stanislava', 'Margaréta', 'Dobroslava', 'Zlatko', 'Anton', 'Vasil', 'Vít', 'Blanka', 'Adolf', 'Vratislav/Vratislava', 'Alfréd', 'Valéria', 'Alojz', 'Paulína', 'Sidónia', 'Ján', 'Tadeáš', 'Adriána', 'Ladislav/Ladislava', 'Beáta', 'Peter a Pavol, Petra', 'Melánia',

      'Diana', 'Berta', 'Miloslav', 'Prokop', 'Sviatok sv. Cyrila a Metoda', 'Patrícia, Patrik', 'Oliver', 'Ivan', 'Lujza', 'Amália', 'Milota', 'Nina', 'Margita', 'Kamil', 'Henrich', 'Drahomír', 'Bohuslav', 'Kamila', 'Dušana', 'Iľja/Eliáš', 'Daniel', 'Magdaléna', 'Oľga', 'Vladimír', 'Jakub', 'Anna/Hana', 'Božena', 'Krištof', 'Marta', 'Libuša', 'Ignác',

      'Božidara', 'Gustáv', 'Jerguš', 'Dominik/Dominika', 'Hortenzia', 'Jozefína', 'Štefánia', 'Oskár', 'Ľubomíra', 'Vavrinec', 'Zuzana', 'Darina', 'Ľubomír', 'Mojmír', 'Marcela', 'Leonard', 'Milica', 'Elena, Helena', 'Lýdia', 'Anabela', 'Jana', 'Tichomír', 'Filip', 'Bartolomej', 'Ľudovít', 'Samuel', 'Silvia', 'Augustín', 'Nikola', 'Ružena', 'Nora',

      'Drahoslava', 'Linda', 'Belo', 'Rozália', 'Regína', 'Alica', 'Marianna', 'Miriama', 'Martina', 'Oleg', 'Bystrík', 'Mária', 'Ctibor', 'Ľubomil, Ľudomil', 'Jolana', 'Ľudmila', 'Olympia', 'Eugénia', 'Konštantín', 'Ľuboslav(a)', 'Matúš', 'Móric', 'Zdenka', 'Ľuboš, Ľubor', 'Vladislav', 'Edita', 'Cyprián', 'Václav', 'Michal, Michaela', 'Jarolím',

      'Arnold', 'Levoslav', 'Stela', 'František', 'Viera', 'Natália', 'Eliška', 'Brigita', 'Dionýz', 'Slavomíra', 'Valentína', 'Maximilián', 'Koloman', 'Boris', 'Terézia', 'Vladimíra', 'Hedviga', 'Lukáš', 'Kristián', 'Vendelín', 'Uršuľa', 'Sergej', 'Alojzia', 'Kvetoslava', 'Aurel', 'Demeter', 'Sabína', 'Dobromila, Kevin', 'Klára', 'Šimon/Šimona', 'Aurélia',

      'Denisa/Denis', 'Pamiatka zosnulých', 'Hubert', 'Karol', 'Imrich', 'Renáta', 'René', 'Bohumír', 'Teodor', 'Tibor', 'Martin, Maroš', 'Svätopluk', 'Stanislav', 'Irma', 'Leopold', 'Agnesa', 'Klaudia', 'Eugen', 'Alžbeta', 'Félix', 'Elvíra', 'Cecília', 'Klement', 'Emília', 'Katarína', 'Kornel', 'Milan', 'Henrieta', 'Vratko', 'Ondrej/Andrej',

      'Edmund', 'Bibiána', 'Oldrich', 'Barbora', 'Oto', 'Mikuláš', 'Ambróz', 'Marína', 'Izabela', 'Radúz', 'Hilda', 'Otília', 'Lucia', 'Branislava, Bronislava', 'Ivica', 'Albína', 'Kornélia', 'Sláva/Slávka', 'Judita', 'Dagmara', 'Bohdan', 'Adela', 'Nadežda', 'Adam a Eva', '1. Sviatok vianočný', 'Štefan', 'Filoména', 'Ivana, Ivona', 'Milada', 'Dávid', 'Silvester');

    // If it's not a leap year, jump ahead after February 28
    if (!$this->isLeapYear() && ($today_yday > 59)) {
      $today_yday++;
    }

    // There is no nameday on leapyears on 02/24
    if (!($this->isLeapYear() && $today_month == 2 && $today_day == 24)) {
      // If it is a leapyear and after 24th of February, then use previous day.
      if ($this->isLeapYear() && $today_month == 2 && $today_day > 24) {
        $today_day--;
      }
      $nameday['hu'] = $name_hu[$today_month][$today_day];
    }
    else {
      $nameday['hu'] = '';
    }

    $nameday['sk'] = $mena[$today_yday];
    if (isset($nameday[$lang])) {
      return $nameday[$lang];
    }

  }

  protected function isLeapYear() {
    return date("Y", time())%4 == 0;
  }

}
