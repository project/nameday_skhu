<?php

/**
 * @file
 * Contains \Drupal\nameday_skhu\Plugin\Block\NamedayBlockSk.
 */

namespace Drupal\nameday_skhu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\nameday_skhu\NamedayClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'Nameday' block.
 *
 * @Block(
 *  id = "nameday_block_sk",
 *  admin_label = @Translation("Slovak nameday block"),
 * )
 */
class NamedayBlockSk extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var $nameday Drupal\nameday_skhu\NamedayClass
   */
  protected $nameday;

  /**
   * @param \Drupal\nameday_skhu\NamedayClass
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, NamedayClass $nameday) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->nameday = $nameday;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('nameday')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];
    $build['#markup'] = "<div class='sk-nameday'> " . $this->nameday->getNameday('sk') . "</div>";
    return $build;
  }

  /**
   * @inheritDoc
   */
  public function getCacheMaxAge() {
    // Do not cache the block.
    return 0;
  }

}
